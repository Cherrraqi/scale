
<style>
h1 { color: blue !important; }
</style>
  
# Création et configuration d'infrastructure sur Azure  avec Terraform et Ansible 

## Cherraqi El Bouazzaoui 

### 05 Avril 2019

----

<style>
h1 { color: blue !important; }
</style>
## Presentation du projet 

* Déploiment d 'un cluster de machines identiques attachée à un load Balancer 
* Utilisation de Ansible pour configurer le cluster des machines


----

## Objectif du projet

* Automatisation du deploiment et la configuration d'infrastructure
* Orchestration avec Jenkins 
-----

### Pourquoi devrions-nous choisir terraform que les autres outils IAC ?

* Configuration Vs orchestration
* Infrastructure Mutable vs Infrastructure Immutable
* Procédural vs Déclaratif


----


## Terraform  ![25%](https://avatars2.githubusercontent.com/u/11051457)
* Outil de creation d'infrastructure as code
* Terraform permet de gérer le cycle de vie des serveurs virtuels
* les fichier de configuration de peuvent etre rédiger en HCL ou en JSON
* Terraform est programmer avec GO


----

## Ansible ![25%](https://keithtenzer.files.wordpress.com/2017/11/ansible-logo.png?w=359&h=284)
* Ansible est une plate-forme logicielle libre pour la configuration et la gestion des serveurs.
* Ansible est programmer en python
* Connexion SSH
* Playbooks
* Roles

-----------


----------------------

## Presentation du code Terraform
* configurer azure Access pour utiliser terraform

```yaml
provider "azurerm" {
subscription_id = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
client_id = "3XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
client_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
tenant_id = "4XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX2"
}

```
----------

## Variables.tf

* Location
* resource_group_name
* admin_user
* admin_password
-----------

## Output.fr

* FQDN du cluster de machines virtuelles
* FQDN de la machine de configuration Jumpbox

```yaml

output "vmss_public_ip" {
     value = "${azurerm_public_ip.vmss.fqdn}"
 }
output "jumpbox_public_ip" {
   value = "${azurerm_public_ip.jumpbox.fqdn}"
}

```
-------------
## Network Infrastructure

* Un seul réseau virtuel avec un espace d 'adressage de 10.0.0.0/16
* Un seul sous réseau avec l 'espace d'adressage 10.0.2.0/24
* Deux adresses IP. Une est utilisé pour le scale set load balancer, et l 'autre est utilisé pour la machine de configuration Jumpbox
----------------


## Cluster des machines virtuelles identiques

* Azure load Balancer attaché à une adresse IP public
* Azure backend address assigner au load balancer
* A health probe configurer dans le load balancer
* 3 Machine Virtuelles (scale set) qui tourneront derrière le load balancer
 
 -----
 
## Machine de Configuration

* une interface réseau connecté au  même sous réseau que les machines identiques scale set
* Jumpbox est accessible à distance via la connection SSH:


----

## Provisionner notre infrastructure
* initialisé le repertoire de travail terraform
``terraform init``
générer et afficher le plan d 'execution de l 'infrastructure
``terraform plan``
Deployer mon infrastructure
``terraform apply ``
![70%](/home/cherraqi/Bureau/img6.png)

--------

## Structure finale du projet

<style>
p { font-size: 10px;  /* Titres de 10 pixels */}
</style>
├── main.tf
├── output.tf
├── terraform.tfstate
├── terraform.tfstate.backup
└── variables.tf
-------

---
## Configuration Avec ansible


```
[webservers]
  10.0.2.8
  10.0.2.9
  10.0.2.10
 10.0.2.8 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
10.0.2.9 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
10.0.2.10 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}

```
----

<style>
p { font-size: 10px; /* Titres de 10 pixels */}
</style>

├── cherraqi.apache2
│   ├── defaults
│   │   └── main.yml
│   ├── files
│   ├── handlers
│   │   └── main.yml
│   ├── meta
│   │   └── main.yml
│   ├── README.md
│   ├── tasks
│   │   └── main.yml
│   ├── templates
│   ├── tests
│   │   ├── inventory
│   │   └── test.yml
│   └── vars
│       └── main.yml
└── cherraqi.necessarytools
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── README.md
    ├── tasks
    │   └── main.yml
    ├── templates
    ├── tests
    │   ├── inventory
    │   └── test.yml
    └── vars
        └── main.yml
        
----

## Dynamic inventory



